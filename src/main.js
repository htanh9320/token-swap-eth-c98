const Web3 = require("web3");
require("dotenv").config();
const { ethers } = require("ethers");
const C98Abi = require("../artifacts/contracts/C98.sol/C98.json");
const BnbAbi = require("../artifacts/contracts/BNB.sol/BNB.json");
const TokenSwapAbi = require("../artifacts/contracts/TokenSwap.sol/TokenSwap.json");
const rpcUrl = process.env.GOERLI_RPC;
const privateKey = process.env.PRIVATE_KEY;
const web3 = new Web3(rpcUrl);
const c98TokenAddress = "0x923f963f85d6575579fC96535c280518a1A70057";
const bnbTokenAddress = "0xC06E546A77eDeaaDC9C35dde29288eB705296e6a";
const contractAddress = "0xc7Eac1982A12B210b6eE2183f9CDCc6B46f22377";
const sender = "0x552f3b71A0743d728e5c621106158134c36efF5e";
const contract = new web3.eth.Contract(TokenSwapAbi.abi, contractAddress);
const amountToken1 = ethers.BigNumber.from("10000000000000000000000");
const amountToken2 = ethers.BigNumber.from("20000000000000000000000");

const approve_token = async (token, amount, receipent) => {
  const token_address = token;
  const token_contract = new web3.eth.Contract(C98Abi.abi, token_address);
  const txn = {
    from: sender,
    to: token_address,
    gas: 500000,
    data: token_contract.methods.approve(receipent, amount).encodeABI(),
  };
  const signature = await web3.eth.accounts.signTransaction(txn, privateKey);
  //   console.log(signature);
  let result = await web3.eth.sendSignedTransaction(signature.rawTransaction);
  console.log(result);
};
const add_liquidity = async () => {
  const txn = {
    from: sender,
    to: contractAddress,
    gas: 500000,
    data: contract.methods.addLiquidity(amountToken1).encodeABI(),
  };
  const signature = await web3.eth.accounts.signTransaction(txn, privateKey);
  let result = await web3.eth.sendSignedTransaction(signature.rawTransaction);
  console.log(result);
};
const swap = async (amount_in) => {
  const account = "0x552f3b71A0743d728e5c621106158134c36efF5e";
  const txn = {
    from: account,
    to: contractAddress,
    gas: 500000,
    data: contract.methods.swap(bnbTokenAddress, amount_in).encodeABI(),
  };
  const signature = await web3.eth.accounts.signTransaction(txn, privateKey);
  let result = await web3.eth.sendSignedTransaction(signature.rawTransaction);
  console.log(result);
};
// approve_token(c98TokenAddress, amountToken1, contractAddress);
// approve_token(bnbTokenAddress, amountToken2, contractAddress);
// add_liquidity();
// const amount_in_swap = ethers.BigNumber.from("100000000000000");
swap(1000);
