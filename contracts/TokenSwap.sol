// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TokenSwap {
    IERC20 public token1;
    IERC20 public token2;

    constructor(address _token1, address _token2) {
        token1 = IERC20(_token1);
        token2 = IERC20(_token2);
    }

    function addLiquidity(uint256 amountToken1) public {
        uint256 amountToken2 = amountToken1 * 2;
        require(
            token1.allowance(msg.sender, address(this)) >= amountToken1 &&
                token2.allowance(msg.sender, address(this)) >= amountToken2,
            "Token allowance too low"
        );
        _safeTransferFrom(token1, msg.sender, address(this), amountToken1);
        _safeTransferFrom(token2, msg.sender, address(this), amountToken2);
    }

    function swap(address input_token, uint256 amount_in) public {
        IERC20 token_in = IERC20(input_token);
        require(
            token_in == token1 || token_in == token2,
            "Faild on receiver input token"
        );
        if (token_in == token1) {
            IERC20 token_out = IERC20(token2);
            require(
                token1.allowance(msg.sender, address(this)) >= amount_in,
                "Token allowance too low"
            );
            uint256 amount_out = amount_in * 2;
            _safeTransferFrom(token_in, msg.sender, address(this), amount_in);
            _safeTransfer(token_out, msg.sender, amount_out);
        } else {
            IERC20 token_out = IERC20(token1);
            require(
                token2.allowance(msg.sender, address(this)) >= amount_in,
                "Token allowance too low"
            );
            uint256 amount_out = amount_in / 2;
            _safeTransferFrom(token_in, msg.sender, address(this), amount_in);
            _safeTransfer(token_out, msg.sender, amount_out);
        }
    }

    function _safeTransferFrom(
        IERC20 token,
        address sender,
        address recipent,
        uint256 amount
    ) private {
        bool sent = token.transferFrom(sender, recipent, amount);
        require(sent, "Token transfer failed");
    }

    function _safeTransfer(
        IERC20 token,
        address recipent,
        uint256 amount
    ) private {
        bool sent = token.transfer(recipent, amount);
        require(sent, "Token transfer failed");
    }
}
