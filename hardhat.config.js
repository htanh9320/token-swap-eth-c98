require("@nomicfoundation/hardhat-toolbox");
const ALCHEMY_API_KEY = "Ppxx_DB7sS96n1Of3EECEilGrhPr9Qb8";
const GOERLI_PRIVATE_KEY =
  "6f018b60eb24631184b46196bfe8590038ad9e313fb74d4663c5a1b30fc9b5e8";
/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.9",
  networks: {
    goerli: {
      url: `https://eth-goerli.g.alchemy.com/v2/${ALCHEMY_API_KEY}`,
      accounts: [`0x${GOERLI_PRIVATE_KEY}`],
    },
  },
};
