const { ethers } = require("hardhat");
async function main() {
  const [deployer] = await ethers.getSigners();
  console.log(deployer);
  console.log("Deploying contracts with the account:", deployer.address);

  const weiAmount = (await deployer.getBalance()).toString();

  console.log("Account balance:", await ethers.utils.formatEther(weiAmount));

  const TokenSwap = await ethers.getContractFactory("TokenSwap");
  const tokenswap = await TokenSwap.deploy(
    "0x923f963f85d6575579fC96535c280518a1A70057",
    "0xC06E546A77eDeaaDC9C35dde29288eB705296e6a"
  );

  console.log("Token address:", tokenswap.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
